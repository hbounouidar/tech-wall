import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-obs',
  templateUrl: './obs.component.html',
  styleUrls: ['./obs.component.css']
})
export class ObsComponent implements OnInit {

  monObs: Observable<any>;
  mesImages = [
    'oim.png',
    'cv.jpg',
    '404.jpg'
  ];
  currentImage: string;
  constructor() { }

  ngOnInit(): void {
     this.monObs = new Observable(
       (observer) => {
         let i = this.mesImages.length - 1;
         setInterval(
           () => {
            observer.next(this.mesImages[i]);
            if( i > 0) {
              i--;
            } else {
               i = this.mesImages.length - 1;
            }
           }
         , 2500);
       }
     );

     this.monObs.subscribe(
       (result) => {
         this.currentImage = result;
       }
     );
  }

}
