import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { Personne } from 'src/app/Model/Personne';


@Component({
  selector: 'app-item-cv',
  templateUrl: './item-cv.component.html',
  styleUrls: ['./item-cv.component.css']
})
export class ItemCvComponent implements OnInit {

  @Input() personne: Personne;
  @Output() selectPersonne = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  onSelectPersonne() {
    this.selectPersonne.emit(
      this.personne
    );
  }

}
