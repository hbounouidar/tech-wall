import { Component, OnInit } from '@angular/core';
import { Personne } from 'src/app/Model/Personne';
import { CvService } from '../cv.service';

@Component({
  selector: 'app-cv',
  templateUrl: './cv.component.html',
  styleUrls: ['./cv.component.css']
})
export class CvComponent implements OnInit {

  personnes: Personne[];
  selectedPersonne: Personne;
  constructor(private cvService: CvService) { }

  ngOnInit(): void {
    this.cvService.getPersonnes().subscribe(
      (personnes) => {
        this.personnes = personnes;
      },
      (error) => {
        alert('Probleme d\'acces à l\'api les données sont fake');
        this.personnes = this.cvService.getFakePersonne();
      }
    );
  }

  onSelectPersonne(pers) {
    this.selectedPersonne = pers;
  }

}
