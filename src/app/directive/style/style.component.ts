import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-style',
  templateUrl: './style.component.html',
  styleUrls: ['./style.component.css']
})
export class StyleComponent implements OnInit {

  @Input() color = 'white';
  @Input() bgColor = 'lightblue';
  size = '25px';
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(
      (params) => {
        this.color = params.default;
      }
    )
  }

  changeSize(size){
    this.size = size + 'px';
  }

  goToCv(){
    const link = ['cv'];
    this.router.navigate(link);
  }

}
