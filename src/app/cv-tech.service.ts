import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CVTechService {

  data = [
    'data1',
    'data2'
  ]
  constructor() { }

  logger(data: any) {
    console.log(this.data);
    console.log(data);
  }

  addData(data) {
    this.data.push(data);
  }
}
